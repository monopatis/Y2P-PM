#ifndef LENOVO_FEATURES_H
#define LENOVO_FEATURES_H

#define R_EC_BATT_PORT 0xEC
#define BATT_LIMIT_ON_EC 0x20
#define BATT_LIMIT_ON_AND_QC_EC 0x24
#define BATT_LIMIT_OFF_EC 0x00
#define R_ED_BATT_PORT 0xED
#define BATT_LIMIT_ON_ED 0x42
#define BATT_LIMIT_OFF_ED 0x40
#define BATT_LIMIT_OFF_DCH_ED 0x02

#define R_E2_QC_PORT 0xE2
#define QC_ON 0x07
#define QC_OFF 0x08

#define R_43_USB_CHARGE 0x43
#define USB_CHARGE_ON 0xC3
#define USB_CHARGE_OFF 0x43

#define N_FEATURES 3
#define LENOVO_BT 0
#define LENOVO_USBC 1
#define LENOVO_QC 2

typedef struct lenovo_features_t {
    void (*set_state)(uint8_t value);
    int (*get_state)(void);
    char* info;
} lenovo_features_t;

void dump_all_regs(void);
int get_charge_threshold_status(void);
void set_charge_threshold(uint8_t value);
int get_usb_charge_status(void);
void set_usb_charge_status(uint8_t value);
int get_quick_charge_status(void);
void set_quick_charge_status(uint8_t value);

#endif
