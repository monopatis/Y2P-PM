# Y2P-PM Power Management Utility for Linux

Y2P-PM is a simple program which allows user to enable/disable  the following Yoga 2 Pro power management features :
  - battery charge threshold (to avoid battery degradation);
  - usb charging of the devices using USB 2.0 port while the laptop is shutdown;
  - quick charge mode (allows to fully charge the battery within 30-40 minutes).

This program is based on the [fanctrl] program and [linux.org.ru] discussion.

### Build process
```sh
$ ./waf configure build
```

### Examples of use
To enable the battery charging threshold:
```sh
# ./y2p-pm -b on
```

To disable the battery charging threshold:
```sh
# ./y2p-pm -b off
```

To get current state of the quick charge feature:
```sh
# ./y2p-pm -q
```

To enable the usb charging while the laptop is turned on:
```sh
# ./y2p-pm -u on
```
It is possible to combine arguments in any order. If neccessary, you can dump the whole EC area using --dump argument.

### Todos
 - Add support for other laptops
 - Add battery calibration routine

[fanctrl]: <http://www.association-apml.fr/upload/fanctrl.c>
[linux.org.ru]: <https://www.linux.org.ru/forum/general/10574293>